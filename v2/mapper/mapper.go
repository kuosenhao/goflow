/*
	1 . Write a function with this signature:

	func CapitalizeEveryThirdAlphanumericChar(s string) string {
		// your code goes here
	}

	that capitalizes *only* every third alphanumeric character of a string, so that if I hand you
	Aspiration.com
	You hand me back
	asPirAtiOn.cOm
	Please note:
	- Characters other than each third should be downcased
	- For the purposes of counting and capitalizing every three characters, consider only alphanumeric
	  characters, ie [a-z][A-Z][0-9].
	2. Do the same problem, but this time create a "mapper" package that looks like this:
*/

package mapper

import (
	"fmt"
	"unicode"
)

// CapitalizeEveryThirdAlphanumericChar returns a string where only the 3rd char if it is
// an alphanumeric char, then it is capitalize.  All other chars are lower-cased
func CapitalizeEveryThirdAlphanumericChar(s string) string {
	result := make([]rune, len(s))
	numOfAlphanumerics := 0
	for i, c := range s {
		if unicode.IsLetter(c) || unicode.IsNumber(c) {
			numOfAlphanumerics++
			// c is a letter.  Add 1 to i to make it 1-based
			if numOfAlphanumerics%3 == 0 {
				// uppercase it since it is a third
				result[i] = unicode.ToUpper(c)
			} else {
				// lowercase it as it is not
				result[i] = unicode.ToLower(c)
			}
		} else {
			result[i] = c
		}
	}
	return string(result)
}

// Interface is the interface for mapper
type Interface interface {
	TransformRune(pos int)
	GetValueAsRuneSlice() []rune
}

// MapString counts alphanumerical runes and capitalize it if it falls on the ordinal number
func MapString(i Interface) {
	for pos := range i.GetValueAsRuneSlice() {
		i.TransformRune(pos)
	}
}

// SkipString stores the ordianl number and turns the input string as []runes.
type SkipString struct {
	runes              []rune
	ordinal            int
	numOfAlphanumerics int
}

// TransformRune uppercase a alphanumeric rune that falls on the ordinal number to uppercase.
// Otherwise, it turns it to lowercase.
func (ss *SkipString) TransformRune(pos int) {
	if unicode.IsLetter(ss.runes[pos]) || unicode.IsNumber(ss.runes[pos]) {
		ss.numOfAlphanumerics++
		if ss.numOfAlphanumerics%ss.ordinal == 0 {
			ss.runes[pos] = unicode.ToUpper(ss.runes[pos])
		} else {
			ss.runes[pos] = unicode.ToLower(ss.runes[pos])
		}
	}
}

// GetValueAsRuneSlice returns a slice of runes representing the input string
func (ss *SkipString) GetValueAsRuneSlice() []rune {
	return ss.runes
}

// NewSkipString creates a new SkipString struct
func NewSkipString(ordinal int, str string) SkipString {
	return SkipString{
		runes:   []rune(str),
		ordinal: ordinal,
	}
}

func (ss SkipString) String() string {
	return fmt.Sprintf("%s", string(ss.runes))
}

func main() {
	s := NewSkipString(3, "Aspiration.com")
	MapString(&s)
	fmt.Println(s)
}
