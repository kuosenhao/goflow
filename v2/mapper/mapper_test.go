package mapper_test

import (
	"testing"

	"bitbucket.org/kuosenhao/goflow/v2/mapper"
)

func TestCapitalizeEveryThirdAlphanumericChar(t *testing.T) {
	s := "Aspiration.com 123"
	exp := "asPirAtiOn.cOm 123"
	result := mapper.CapitalizeEveryThirdAlphanumericChar(s)
	if exp != result {
		t.Fatalf("exp %s != result %s", exp, result)
	}

	s = "_Aspiration.com 123"
	exp = "_asPirAtiOn.cOm 123"
	result = mapper.CapitalizeEveryThirdAlphanumericChar(s)
	if exp != result {
		t.Fatalf("exp %s != result %s", exp, result)
	}
}

func TestSkipString(t *testing.T) {
	s := mapper.NewSkipString(3, "Aspiration.com")
	exp := "asPirAtiOn.cOm"
	mapper.MapString(&s)
	if exp != s.String() {
		t.Fatalf("exp %s != s.String() %s", exp, s.String())
	}
}
