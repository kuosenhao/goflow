package utils

import (
	"testing"
)

func TestVersion(t *testing.T) {
	want := "v1.0.0"
	version := Version()
	if want != version {
		t.Fatalf(`Version() %s != %s`, version, want)
	}
}
