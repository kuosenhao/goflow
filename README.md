# Goflow: A simple project to test Go Modules

## Add Files and Enable Go Module

First let's add files 

- `greetings.go`  (package `goflow`)
- `greetings_test.go` (package `goflow`)
- `utiles/utils.go` (package `utils`)

Run this command to initialize go module for this project.
`go mod init bitbucket.org/kuosenhao/goflow`

## goflow Package v0
### Using the goflow Package (v0)

To use the follow package, for example for project `greetingscaller`, one needs to

- import

  - `"bitbucket.org/kuosenhao/goflow"` 
  - `"bitbucket.org/kuosenhao/goflow/utils"` 

- run `go get -u bitbucket.org/kuosenhao/goflow` to install the package to `$GOPATH/pkg/mod/bitbucket/kuosenhao/goflow@v0.0.0-20210311231151-0049567aed56`.  If no version is specified, go generates `v0.0.0-20210311231151-0049567aed56` as the pseudo version.

### Upgrade to the latest v0 version of the goflow Package 

`go get -u bitbucket.org/kuosenhao/goflow@v1.0.0` will upgrade the package to `$GOPATH/pkg/mod/bitbucket/kuosenhao/goflow@v0.0.0-20210311231151-bbc19ba39af9`

## gloflow Package v1

First the `goflow` repo must be tag a version, i.e. `v1.0.0`.  Tagging a version is typically done on the `master` branch.

In project `greetingscaller`, run the following command:
`go get -u bitbucket.org/kuosenhao/goflow@v1.0.0` will upgrade the package to `$GOPATH/pkg/mod/bitbucket/kuosenhao/goflow@v1.0.0`.  It will also upgrade `go.mod` and `go.sum`.

## goflow Package v2 and above

### A Separate v2 directory

For version v2 and above, it is recommended to create a `v2` directory and move all `*.go` files into it.  
Then copy `go.mod` to `v2/go.mod`.  Change module name to `bitbucket.org/kuosenhao/goflow/v2`.

Cd to `v2` and run `go clean; go build; gotest` and go will build and test codes under the `v2` dir.

### Tagging the v2 version

Run these command `git tag v2.0.0-alpha.1; git push origin v2.0.0-alpha.1` to tag the version for the code
in `v2/` as `v2.0.0-alpha.1` for a v2 version that is still undergoing development.  When the code has
been done and tested, then tag the code as `v2.0.0`.  When go retrieves the version for v2, `v2.0.0` will
be given a high priority than `v2.0.0-alpha.1`.  

### Import the v2 version

In project `greetingscaller`, change the imports to 

  - `"bitbucket.org/kuosenhao/goflow/v2"` 
  - `"bitbucket.org/kuosenhao/goflow/v2/utils/"` 

Running `go test ./...` will download the latest v2 version.

To use `v2.0.0` instead of `v2.0.0-alpha.1`, manually update `go.mod` and set the v2 version from 
`v2.0.0-alpha.1` to `v2.0.0`.  Run `go clean; go build` to download the source code for `v2.0.0`.
